TAG=$(shell head -n1 .release)
USERNAME=iudanet
NAME=$(shell basename $(CURDIR))
BRANCH=$(shell git symbolic-ref --short -q HEAD)
COMMIT=$(shell git rev-parse HEAD)
IMAGE=$(USERNAME)/$(NAME)
build::
	docker build \
		-t $(IMAGE):$(TAG) \
		-t $(IMAGE):$(BRANCH) \
		-t $(IMAGE):$(COMMIT) \
		. 


push:: build
	docker push $(IMAGE):$(TAG)
	docker push $(IMAGE):$(BRANCH)
	docker push $(IMAGE):$(COMMIT)
