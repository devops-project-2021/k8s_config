FROM alpine:3.13.4


RUN apk add --no-cache --update \
     curl bash make unzip jq && \
    adduser k8s \
        -h /home/k8s \
        -D \
        -s /bin/bash \
        -u 3000

WORKDIR /home/k8s

# install yc
RUN curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash && \
    mv /root/yandex-cloud/bin/yc /usr/local/bin/ && \
    yc version

# install kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && \
    kubectl version --client=true

# install helm 3.5.3
RUN curl https://get.helm.sh/helm-v3.5.3-linux-amd64.tar.gz | tar zx && \
    mv linux-amd64/helm /usr/bin/ && \
    helm version

RUN curl -LO "https://releases.hashicorp.com/terraform/0.14.10/terraform_0.14.10_linux_amd64.zip" && \
    unzip terraform_0.14.10_linux_amd64.zip && \
    rm terraform_0.14.10_linux_amd64.zip && \
    mv terraform /usr/local/bin/

USER k8s